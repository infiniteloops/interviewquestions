﻿
namespace InterviewQuestions.Interfaces
{
    public interface IArrayCreator<out T>
    {
        T[] Array { get; }

        T[] CreateArray(int size, int min, int max);
        T[,] CreateArray(int height, int width);
    }
}
