﻿using System;

namespace InterviewQuestions.Interfaces
{
    public interface IQuestion
    {
        /// <summary>
        /// Run Answer to implementing question
        /// </summary>
        /// <param name="output">Output delegate used to display information back to the user</param>
        void Answer(Action<string> output);
    }
}
