﻿
namespace InterviewQuestions.FSharpSolutions

open System.Collections.Generic
open InterviewQuestions.Interfaces

(*  Tree visualization
 *  ------------------
 *          1
 *          /\
 *    2          3 
 *    /\         /\
 *   4   5     -    6
 *  /\   /\    /\   /\
 *  8-  9 10  - -  -  11
 *)

type Tree = 
    | Tip
    | Node of int * Tree * Tree

type ValueWithNext =
    | Value of int * int option

type BinaryTreeQ() =     
    interface IQuestion with
        member this.Answer action =
            let traversedTree = this.BFTravel this.StandardTree
            let rec printer tree =
                match tree with
                | [] -> ()
                | Value(v,n)::t ->
                    let next =
                        match n with
                        | Some x-> x.ToString()
                        | None -> "Empty"
                    action.Invoke(System.String.Format("{0} -> {1}", v, next))
                    printer t
            printer traversedTree
    member this.Answer action = (this :> IQuestion).Answer action
    member this.StandardTree = 
        Node(1, 
            Node(2, 
                Node(4, Node(8, Tip, Tip), Tip), 
                Node(5, Node(9, Tip, Tip), Node(10, Tip, Tip))), 
            Node(3, 
                Tip, 
                Node(6, Tip, Node(11, Tip, Tip))))
    member this.MoveQueue (q1:Queue<'T>) (q2:Queue<'T>) =
        if(q1.Count = 0) then 
            () 
        else
            while q1.Count > 0 do
                q2.Enqueue(q1.Dequeue())
    member this.EnqueueNodes (queue:Queue<Tree>) tree =
        match tree with
        | Tip -> ()
        | Node(_,_,_) -> queue.Enqueue(tree)
    member this.BuildValues (result:ResizeArray<ValueWithNext>) queueNext current next =
        match current with
            | Tip -> ()
            | Node(v,l,r) -> 
                match next with
                | None -> result.Add(Value(v,None))
                | Some(Tip) -> ()
                | Some(Node(v2,_,_)) -> result.Add(Value(v,Some(v2)))
                queueNext l
                queueNext r 
    member this.BFTravel tree =
        let currentLevel = new Queue<Tree>()
        currentLevel.Enqueue(tree)
        let nextLevel =  new Queue<Tree>()
        let queueNext = this.EnqueueNodes nextLevel
        let result = new ResizeArray<ValueWithNext>()
        let builder = this.BuildValues result queueNext
        while currentLevel.Count > 0 do
            let current = currentLevel.Dequeue()
            let next = if currentLevel.Count > 0 then Some(currentLevel.Peek()) else None
            builder current next   
            if currentLevel.Count = 0 then this.MoveQueue nextLevel currentLevel else ()
        result |> Seq.toList
    member this.DFTravel tree =   
        let arr = new ResizeArray<int>()
        let rec walker tree =
            match tree with
            | Tip -> ()
            | Node(v,l,r) ->  
                arr.Add(v)                              
                walker l
                walker r
        walker tree
        arr |> Seq.toList
