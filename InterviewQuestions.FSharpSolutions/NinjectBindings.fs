﻿namespace InterviewQuestions.NinjectBindings

open Ninject
open Ninject.Modules
open InterviewQuestions.Interfaces
open InterviewQuestions.FSharpSolutions

type QuestionBinding()=
    inherit NinjectModule()
    override this.Load()=
        this.Kernel.Bind<IQuestion>().To<BinaryTreeQ>().Named("BinaryFSharp") |> ignore
        ()