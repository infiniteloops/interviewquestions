﻿// Learn more about F# at http://fsharp.net. See the 'F# Tutorial' project
// for more guidance on F# programming.
#r @"bin\debug\InterviewQuestions.Interfaces.dll"
#load "BinaryTreeQ.fs"
open InterviewQuestions.FSharpSolutions

let q = BinaryTreeQ()
q.Answer (fun a -> printfn "%s" a)

// Define your library scripting code here

