﻿using System;
using System.Diagnostics;
using InterviewQuestions.Interfaces;
using Ninject;
using ServiceStack.Logging;

namespace InterviewQuestions.Console
{
    class Program
    {
        private static readonly IKernel Kernel;
        private static readonly ILog Log;

        static Program()
        {
            //Hello
            Kernel = new StandardKernel();
            Kernel.Load("Interview*.dll");
            Log = Kernel.Get<ILog>();
        }

        static void Main(string[] args)
        {
            SetDisplay();
            RunUntimedAnswers();
            RunTimedAnswers();         
        }

        private static void RunUntimedAnswers()
        {
            var question = Kernel.Get<IQuestion>("Binary");
            question.Answer(Log.Info);
            System.Console.ReadKey(true);

            System.Console.Clear();
            question = Kernel.Get<IQuestion>("BinaryFSharp");
            question.Answer(Log.Info);
            System.Console.ReadKey(true);

            System.Console.Clear();
            question = Kernel.Get<IQuestion>("ArrayPrint");
            question.Answer(Log.Info);
            System.Console.ReadKey(true);

            System.Console.Clear();
            question = Kernel.Get<IQuestion>("Scrabble");
            question.Answer(Log.Info);
            System.Console.ReadKey(true);

            System.Console.Clear();
        }

        private static void RunTimedAnswers()
        {
            var sw = new Stopwatch();

            // Run Sorted list first (this will also instantiate the array creator and the integer array
            // The array will be reused in the second answer (singleton lifetime) so as to provide the same number of matches
            // This will cause extra overhead for the sorted answer
            sw.Start();
            var question = Kernel.Get<IQuestion>("ArrayComboSorted");
            question.Answer(Log.Debug);
            sw.Stop();

            Log.Info(string.Format("Sorted took: {0} ms", sw.ElapsedMilliseconds));
            System.Console.ReadKey(true);
            System.Console.WriteLine();

            sw.Reset();
            sw.Start();
            question = Kernel.Get<IQuestion>("ArrayCombo");
            question.Answer(Log.Debug);
            sw.Stop();

            Log.Info(string.Format("Unsorted took: {0} ms", sw.ElapsedMilliseconds));
            System.Console.ReadKey(true);
        }

        private static void SetDisplay()
        {
            System.Console.BackgroundColor = ConsoleColor.Black;
            System.Console.ForegroundColor = ConsoleColor.Yellow;
            System.Console.Clear();
        }
    }
}
