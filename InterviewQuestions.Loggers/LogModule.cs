﻿using Ninject.Modules;
using ServiceStack.Logging;
using ServiceStack.Logging.Support.Logging;

namespace InterviewQuestions.Loggers
{
    public class LogModule : NinjectModule
    {
        public override void Load()
        {
            LogManager.LogFactory = new DebugFactory(new ConsoleLogFactory());
            Kernel.Bind<ILog>().ToMethod(ctx => LogManager.GetLogger((ctx.Request.Target ?? new object()).GetType()));
        }
    }
}
