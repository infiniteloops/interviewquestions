﻿using System;
using ServiceStack.Logging;

namespace InterviewQuestions.Loggers
{
    public class DebugFactory : ILogFactory
    {
        private readonly ILogFactory _wrappedFactory;
        public DebugFactory(ILogFactory wrappedFactory)
        {
            this._wrappedFactory = wrappedFactory;
        }
        public ILog GetLogger(string typeName)
        {
            return new DebugLogger(typeName, _wrappedFactory.GetLogger(typeName));
        }

        public ILog GetLogger(Type type)
        {
            return new DebugLogger(type, _wrappedFactory.GetLogger(type));
        }
    }


    /// <summary>
    /// Default logger is to System.Diagnostics.Debug.WriteLine
    /// 
    /// Made public so its testable
    /// </summary>
    public class DebugLogger : ILog
    {
        const string DEBUG = "DEBUG: ";
        const string ERROR = "ERROR: ";
        const string FATAL = "FATAL: ";
        const string INFO = "INFO: ";
        const string WARN = "WARN: ";
        private static ILog _wrapped;

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugLogger"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="wrapped"></param>
        public DebugLogger(string type, ILog wrapped)
        {
            _wrapped = wrapped;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugLogger"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="wrapped"></param>
        public DebugLogger(Type type, ILog wrapped)
        {
            _wrapped = wrapped;
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        private static void Log(object message, Exception exception)
        {
            string msg = message == null ? string.Empty : message.ToString();
            if (exception != null)
            {
                msg += ", Exception: " + exception.Message;
            }
            System.Diagnostics.Debug.WriteLine(msg);            
        }

        /// <summary>
        /// Logs the format.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        private static void LogFormat(object message, params object[] args)
        {
            string msg = message == null ? string.Empty : message.ToString();
            System.Diagnostics.Debug.WriteLine(msg, args);
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        private static void Log(object message)
        {
            string msg = message == null ? string.Empty : message.ToString();
            System.Diagnostics.Debug.WriteLine(msg);
        }

        public void Debug(object message, Exception exception)
        {
            Log(DEBUG + message, exception);
            _wrapped.Debug(message, exception);
        }

        public bool IsDebugEnabled { get { return true; } }

        public void Debug(object message)
        {
            Log(DEBUG + message);
            _wrapped.Debug(message);
        }

        public void DebugFormat(string format, params object[] args)
        {
            LogFormat(DEBUG + format, args);
            _wrapped.DebugFormat(format, args);
        }

        public void Error(object message, Exception exception)
        {
            Log(ERROR + message, exception);
            _wrapped.Error(message, exception);
        }

        public void Error(object message)
        {
            Log(ERROR + message);
            _wrapped.Error(message);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            LogFormat(ERROR + format, args);
            _wrapped.ErrorFormat(format, args);
        }

        public void Fatal(object message, Exception exception)
        {
            Log(FATAL + message, exception);
            _wrapped.Fatal(message, exception);
        }

        public void Fatal(object message)
        {
            Log(FATAL + message);
            _wrapped.Fatal(message);
        }

        public void FatalFormat(string format, params object[] args)
        {
            LogFormat(FATAL + format, args);
            _wrapped.FatalFormat(format, args);
        }

        public void Info(object message, Exception exception)
        {
            Log(INFO + message, exception);
            _wrapped.Info(message, exception);
        }

        public void Info(object message)
        {
            Log(INFO + message);
            _wrapped.Info(message);
        }

        public void InfoFormat(string format, params object[] args)
        {
            LogFormat(INFO + format, args);
            _wrapped.InfoFormat(format, args);
        }

        public void Warn(object message, Exception exception)
        {
            Log(WARN + message, exception);
            _wrapped.Warn(message, exception);
        }

        public void Warn(object message)
        {
            Log(WARN + message);
            _wrapped.Warn(message);
        }

        public void WarnFormat(string format, params object[] args)
        {
            LogFormat(WARN + format, args);
            _wrapped.WarnFormat(format, args);
        }
    }
}