﻿using System.Diagnostics;
using System.Web.Mvc;
using InterviewQuestions.Interfaces;
using Ninject;
using ServiceStack.Logging;

namespace InterviewQuestions.MVC.Controllers
{
    public class DefaultController : Controller
    {
        [Inject]
        public IKernel Container { get; set; }
        [Inject]
        public ILog Log { get; set; }

        private void WriteLine(string result = "")
        {
            string output = result + "<br/>";
            Log.Debug(output);
            Response.Write(output);
        }

        private void Write(string result)
        {
            string output = result.Replace("\r", "<br/>");
            Log.Debug(output);
            Response.Write(output);
        }

        //
        // GET: /Default/
        public ActionResult Index()
        {
            UntimedAnswers();
            TimedAnswers();
            return View();
        }

        private void TimedAnswers()
        {
            Stopwatch sw = new Stopwatch();

            // Run Sorted list first (this will also instantiate the array creator and the integer array
            // The array will be reused in the second answer (singleton lifetime) so as to provide the same number of matches
            // This will cause extra overhead for the sorted answer
            sw.Start();
            var question = Container.Get<IQuestion>("ArrayComboSorted");
            question.Answer(WriteLine);
            sw.Stop();

            WriteLine(string.Format("Sorted took: {0, 20}", sw.Elapsed.ToString()));
            WriteLine();

            sw.Reset();
            sw.Start();
            question = Container.Get<IQuestion>("ArrayCombo");
            question.Answer(WriteLine);
            sw.Stop();

            WriteLine(string.Format("Unsorted took: {0, 18}", sw.Elapsed.ToString()));
        }

        private void UntimedAnswers()
        {            
            var question = Container.Get<IQuestion>("Binary");
            question.Answer(WriteLine);
            WriteLine();
            question = Container.Get<IQuestion>("ArrayPrint");
            question.Answer(Write);
            WriteLine();
            question = Container.Get<IQuestion>("Scrabble");
            question.Answer(WriteLine);
            WriteLine();
        }

    }
}
