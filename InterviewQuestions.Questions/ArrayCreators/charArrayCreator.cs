﻿using System;
using InterviewQuestions.Interfaces;

namespace InterviewQuestions.CSharpSolutions.ArrayCreators
{
    public class CharArrayCreator : IArrayCreator<char>
    {
        public char[] CreateArray(int size, int min, int max)
        {
            char[] array = new char[size];

            Random rnd = new Random();
            for (int i = 0; i < size; i++)
            {
                char c = Convert.ToChar(rnd.Next(min, max));
                array[i] = c;
            }

            return array;
        }

        public char[,] CreateArray(int height, int width)
        {
            //hard coded for easy verification
            char[,] array = new char[8, 8];

            // carrot
            array[0, 5] = 'c';
            array[1, 5] = 'a';
            array[2, 5] = 'r';
            array[3, 5] = 'r';
            array[4, 5] = 'o';
            array[5, 5] = 't';

            // cap
            array[0, 6] = 'a';
            array[0, 7] = 'p';

            // ate at
            array[1, 6] = 't';
            array[2, 6] = 'e';

            // on
            array[4, 6] = 'n';

            //to no
            array[5, 6] = 'o';

            // cart
            array[2, 3] = 'c';
            array[2, 4] = 'a';

            // candy
            array[3, 3] = 'a';
            array[4, 3] = 'n';
            array[5, 3] = 'd';
            array[6, 3] = 'y';

            //sand
            array[5, 0] = 's';
            array[5, 1] = 'a';
            array[5, 2] = 'n';

            //sew
            array[6, 0] = 'e';
            array[7, 0] = 'w';

            return array;
        }

        public char[] Array
        {
            get { throw new NotImplementedException(); }
        }
    }
}
