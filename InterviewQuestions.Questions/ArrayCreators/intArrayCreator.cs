﻿using System;
using System.Linq;
using InterviewQuestions.Interfaces;

namespace InterviewQuestions.CSharpSolutions.ArrayCreators
{    
    public class IntArrayCreator : IArrayCreator<int>
    {
        private int[] _array;

        /// <summary>
        /// Create an array of the provided size using random ints for each item
        /// </summary>
        /// <param name="size">Array size</param>
        /// <param name="min">minimum random value</param>
        /// <param name="max">maximum random value</param>
        /// <returns>Random generated array</returns>
        public int[] CreateArray(int size, int min, int max)
        {
            // create array if none exists
            if (_array == null || !_array.Any())
            {
                _array = new int[size];
                Random rnd = new Random();
                //populate array with random values
                for (int i = 0; i < size; i++)
                {
                    _array[i] = rnd.Next(min, max);
                }                
            }
            return _array;
        }


        public int[,] CreateArray(int height, int width)
        {
            throw new NotImplementedException();
        }

        public int[] Array
        {
            get 
            {
                if (_array == null)
                {
                    //Create default
                    _array = CreateArray(50000, 0, 100);
                }
                return _array; 
            }
        }
    }
}
