﻿using System;
using InterviewQuestions.Interfaces;

namespace InterviewQuestions.CSharpSolutions
{
    public class ArrayFindComboQ : IQuestion
    {
        private readonly int[] _array;

        public ArrayFindComboQ(IArrayCreator<int> arrayCreater)
        {
            _array = arrayCreater.Array;
        }

        public void Answer(Action<string> output)
        {
            output("Array Find Combo Answer O(n^2)");
            output("==============================");
            Int64 loopCount = 0;
            Int64 matches = 0;
            // loop from 0 to length and i+1 to length to hit all combinations of values
            for (int i = 0; i < _array.Length; i++)
            {
                for (int j = i + 1; j < _array.Length; j++)
                {
                    loopCount++;
                    if (_array[i] + _array[j] == 12)
                    {
                        matches++;
                        //foundIndexes.Add(new KeyValuePair<int, int>(i, j));
                        //output(string.Format("Indexes: {0}, {1} - Values: {2}, {3}", i, j, array[i], array[j]));
                    }
                }
            }

            output(String.Format("Array Size: {0, 21}", _array.Length.ToString("N0")));
            output(String.Format("Loops:{0, 27}", loopCount.ToString("N0")));
            output(String.Format("Total matches:{0, 19}", matches.ToString("N0")));
        }
    }
}
