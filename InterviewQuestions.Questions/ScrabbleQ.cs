﻿using System;
using System.Collections.Generic;
using InterviewQuestions.Interfaces;

namespace InterviewQuestions.CSharpSolutions
{    
    public class ScrabbleQ : IQuestion
    {
        readonly char[,] _array;
        private const int Size = 8;

        public ScrabbleQ(IArrayCreator<char> arrayCreator)
        {
            _array = arrayCreator.CreateArray(Size, Size);
        }

        public void Answer(Action<string> output)
        {
            output("Scrabble Word Find Answer");
            output("=========================");
            output("");
            output("Board:");
            PrintBoard(output);
            
            var words = new List<string>();
            FindWords(words);

            output("");
            output("Words found");
            output("===========");
            foreach (string word in words)
            {
                output(word);
            }
        }

        private void PrintBoard(Action<string> output)
        {
            var rowText = string.Empty;
            for (var row = 0; row < Size; row++)
            {
                for (var col = 0; col < Size; col++)
                {
                    rowText += _array[row, col] != '\0' ? _array[row, col] : '-';
                    rowText += ' ';
                }
                output(rowText);
                rowText = string.Empty;
            }
        }

        /// <summary>
        /// loop through board array doing both horizontal and vertical simultaneously
        /// </summary>
        /// <param name="words">List is populated by found words</param>
        private void FindWords(List<string> words)
        {
            for (var i = 0; i < Size; i++)
            {
                var wordH = string.Empty;
                var wordV = string.Empty;
                for (var j = 0; j < Size; j++)
                {
                    // if letter exists add to temp word variable
                    // if not add word variable to list if word is greater than 1 char long
                    if (_array[i, j] != '\0')
                    {
                        wordH += _array[i, j];
                        // if last letter of row check to see if word exists
                        if (j == Size - 1)
                        {
                            CheckWord(words, wordH);
                        }
                    }
                    else
                    {
                        CheckWord(words, wordH);
                        wordH = string.Empty;
                    }

                    // if letter exists add to temp word variable
                    // if not add word variable to list if word is greater than 1 char long
                    if (_array[j, i] != '\0')
                    {
                        wordV += _array[j, i];
                        // if last letter of column check to see if word exists
                        if (j == Size - 1)
                        {
                            CheckWord(words, wordV);
                        }
                    }
                    else
                    {
                        CheckWord(words, wordV);
                        wordV = string.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Check to see if word provided fulfills requirements (greater than 1 char in length)
        /// </summary>
        /// <param name="words">list to add word to</param>
        /// <param name="word">string to check for word</param>
        private static void CheckWord(ICollection<string> words, string word)
        {
            if (word.Length > 1)
            {
                words.Add(word);
            }
        }
    }
}
