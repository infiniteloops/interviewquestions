﻿using System;
using System.Collections.Generic;
using InterviewQuestions.Interfaces;

namespace InterviewQuestions.CSharpSolutions
{
    public class BinaryTreeQ : IQuestion
    {
        private class Node
        {
            public int Id { get; set; }
            public Node Left { get; set; }
            public Node Right { get; set; }
            public Node Next { get; set; }
        }

        private Node _head;

        public BinaryTreeQ()
        {
            CreateBinaryTree();
        }

        private void CreateBinaryTree()
        {
            /*  Tree visualization
             *  ------------------
             *          1
             *          /\
             *    2          3 
             *    /\         /\
             *   4   5     -    6
             *  /\   /\    /\   /\
             *  8-  9 10  - -  -  11
             */

            var first = new Node
                {
                    Id = 1,
                    Left = new Node {Id = 2},
                    Right = new Node {Id = 3}
                };

            // Set children for 1

            // Set children for 2
            first.Left.Left = new Node { Id = 4 };
            first.Left.Right = new Node { Id = 5 };

            // Set children for 3                     
            first.Right.Right = new Node { Id = 6 };

            // Set children for 4
            first.Left.Left.Left = new Node { Id = 8 };

            // Set children for 5
            first.Left.Right.Left = new Node { Id = 9 };
            first.Left.Right.Right = new Node { Id = 10 };

            // Set children for 6
            first.Right.Right.Right = new Node { Id = 11 };

            // Set head for processing
            _head = first;            
        }

        public void Answer(Action<string> output)
        {
            output("Binary Tree Traversal Answer");
            output("============================");
            var q = new Queue<Node>();
            var q2 = new Queue<Node>();
            q.Enqueue(_head);
            output("Node\t->\tNode.Next");
            output("=========================");
            while (q.Count > 0)
            {
                Node n = q.Dequeue();
                n.Next = q.Count > 0 ? q.Peek() : null;
                output(string.Format("{0}\t->\t{1}", n.Id, n.Next != null ? n.Next.Id.ToString() : "Empty"));

                AddNextLevelToQueue(q2, n);
                MoveQ2ToQ(q2, q);
            }
        }

        private static void MoveQ2ToQ(Queue<Node> q2, Queue<Node> q)
        {
            if (q.Count == 0 && q2.Count > 0)
            {
                // O(n) time for this loop but no extra space required 
                // (vs Q = Q2; Q2 = new Queue<>()); with lower time requirement but larger potential space requirement
                while (q2.Count > 0)
                {
                    q.Enqueue(q2.Dequeue());
                }
            }
        }

        private static void AddNextLevelToQueue(Queue<Node> q2, Node n)
        {
            if (n.Left != null)
            {
                q2.Enqueue(n.Left);
            }

            if (n.Right != null)
            {
                q2.Enqueue(n.Right);
            }
        }
    }
}
