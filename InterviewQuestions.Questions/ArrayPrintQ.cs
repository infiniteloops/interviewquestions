﻿using System;
using InterviewQuestions.Interfaces;

namespace InterviewQuestions.CSharpSolutions
{
    public class ArrayPrintQ : IQuestion
    {
        private readonly char[] _array;

        public ArrayPrintQ(IArrayCreator<char> arrayCreator)
        {
            _array = arrayCreator.CreateArray(20, 65, 91);
        }

        public void Answer(Action<string> output)
        {
            Array.Sort(_array);
            output("Array Print Answer");
            output(Environment.NewLine);
            output("==================");
            output(Environment.NewLine);
            PrintArray(_array, 3, PrintOrientation.Column, output);
            output(Environment.NewLine);
            PrintArray(_array, 3, PrintOrientation.Row, output);
        }

        private void PrintArray(char[] array, int count, PrintOrientation orientation, Action<string> output)
        {
            var columns = 0;
            var rows = 0;

            switch (orientation)
            {
                case PrintOrientation.Column:
                    columns = count;
                    rows = (array.Length / count) + 1;
                    break;
                case PrintOrientation.Row:
                    rows = count;
                    columns = (array.Length / count) + 1;
                    break;
            }

            for (var row = 0; row < rows; row++)
            {
                for (var col = 0; col < columns; col++)
                {
                    var ix = orientation == PrintOrientation.Row ? (col * count) + row : col + (row * count);
                    if (ix < array.Length)
                    {
                        output(array[ix] + "\t");
                    }
                    
                }
                output(Environment.NewLine);
            }
            
        }

        private enum PrintOrientation
        {
            Column,
            Row
        }
    }
}
