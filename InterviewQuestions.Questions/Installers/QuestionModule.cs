﻿using InterviewQuestions.CSharpSolutions.ArrayCreators;
using InterviewQuestions.Interfaces;
using Ninject.Modules;

namespace InterviewQuestions.CSharpSolutions.Installers
{
    /// <summary>
    /// This class registers all the questions and array creators for the system
    /// </summary>
    public class QuestionModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IArrayCreator<int>>().To<IntArrayCreator>().InSingletonScope();
            Kernel.Bind<IArrayCreator<char>>().To<CharArrayCreator>().InSingletonScope();
            Kernel.Bind<IQuestion>().To<BinaryTreeQ>().Named("Binary");
            Kernel.Bind<IQuestion>().To<ArrayFindComboQ>().Named("ArrayCombo");
            Kernel.Bind<IQuestion>().To<ArrayFindComboSortedQ>().Named("ArrayComboSorted");
            Kernel.Bind<IQuestion>().To<ArrayPrintQ>().Named("ArrayPrint");
            Kernel.Bind<IQuestion>().To<ScrabbleQ>().Named("Scrabble");
        }
    }
}
