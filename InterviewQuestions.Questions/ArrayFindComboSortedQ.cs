﻿using System;
using InterviewQuestions.Interfaces;

namespace InterviewQuestions.CSharpSolutions
{
    public class ArrayFindComboSortedQ : IQuestion
    {
        private readonly int[] _array;

        public ArrayFindComboSortedQ(IArrayCreator<int> arrayCreater)
        {
            _array = arrayCreater.Array;
        }

        public void Answer(Action<string> output)
        {
            output("Array Find Combo Sorted Answer O(n log(n))");
            output("==========================================");
            // use array.sort (quick sort, low overhead)
            Array.Sort(_array);
            var lowerIx = 0;
            var upperIx = _array.Length - 1;
            var loopCount = 0L;
            var matches = 0L;
            var upperIxHold = -1;
            const int find = 12;

            // while not at the pivot and no hold value is assigned
            while (lowerIx < upperIx || upperIxHold != -1)
            {
                loopCount++;
                // if a match is found add to results and check for duplicate values
                if (_array[lowerIx] + _array[upperIx] == find)
                {
                    matches++;                    
                    // if next item in array is the same as the previous (and doesn't cross the pivot)
                    // set the hold value so we can set it back for the next lower item
                    if (_array[upperIx] == _array[upperIx -1] && (upperIx - 1) > lowerIx)
                    {
                        if (upperIxHold == -1)
                        {
                            upperIxHold = upperIx;
                        }

                        upperIx--;
                        continue;
                    }                    

                    // force next iteration if we reach this point
                    if (upperIxHold != -1)
                    {
                        upperIx = upperIxHold;
                        upperIxHold = -1;
                    }

                    lowerIx++;                    
                }
                // if current value is less than the find value then move lowerIx up
                // [0, 1, 2, 3, 4] and ix = 0, then ix++ will increase value
                else if (_array[lowerIx] + _array[upperIx] < find)
                {
                    lowerIx++;
                }
                // if current value is greater than the find value then move upperIx down
                // [0, 1, 2, 3, 4] and ix = 4, then ix-- will decrease value
                else
                {
                    upperIx--;
                }
            }
            output(String.Format("Array Size: {0, 21}", _array.Length.ToString("N0")));
            output(String.Format("Loops:{0, 27}", loopCount.ToString("N0")));
            output(String.Format("Total matches:{0, 19}", matches.ToString("N0")));
        }
    }
}
